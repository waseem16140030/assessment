import React from "react";
import { Select } from "antd";
const SelectBox = () => {
 const { Option } = Select;
 function handleChange(value) {
  console.log(`selected ${value}`);
 }
 return (
  <div>
   <Select
    placeholder='Chose one'
    onChange={handleChange}
    style={{ width: "130px" }}
   >
    <Option value='1'>Option 1</Option>
    <Option value='2'>Option 2</Option>
    <Option value='3'>Option 3</Option>
   </Select>
  </div>
 );
};

export default SelectBox;
