import React from "react";
import { Input } from "antd";
import SelectBox from "./SelectBox";
import SearchBtn from "./SearchBtn";
const TopSearchBar = () => {
 return (
  <div>
   <Input
    placeholder='Search for anything...'
    className='custom-search-box'
    addonBefore={<SelectBox />}
    addonAfter={<SearchBtn />}
   />
  </div>
 );
};

export default TopSearchBar;
