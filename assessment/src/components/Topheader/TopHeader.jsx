import React from "react";
import styles from "./styles.module.scss";
import TopSearchBar from "./TopSearchBar";
const TopHeader = ({ toggler }) => {
 return (
  <div className={styles["flex-header"]}>
   <div>
    <div>{toggler()}</div>
   </div>
   <div className={styles["ml-50"]}>
    <TopSearchBar />
   </div>
  </div>
 );
};

export default TopHeader;
