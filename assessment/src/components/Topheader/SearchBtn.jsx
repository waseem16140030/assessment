import React from "react";
import { Button } from "antd";
import { SearchOutlined } from "@ant-design/icons";
const SearchBtn = () => {
 return (
  <div>
   <Button className='custom-searchBtn'>
    <SearchOutlined />
   </Button>
  </div>
 );
};

export default SearchBtn;
