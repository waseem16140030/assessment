import React, { useState } from "react";
import { Layout, Menu, Button, Badge } from "antd";
import styles from "./styles.module.scss";
import {
 AlignLeftOutlined,
 AlignRightOutlined,
 HomeOutlined,
 WalletOutlined,
 AppstoreOutlined,
 ShoppingCartOutlined,
 MailOutlined,
} from "@ant-design/icons";
import logo from "../../assets/images/logo.PNG";
import TopHeader from "../Topheader/TopHeader";
const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;
const SideBar = (props) => {
 const [collapsed, setCollapsed] = useState(false);
 const handleCollapsed = () => {
  setCollapsed(!collapsed);
 };
 const togglerBtn = () => {
  return (
   <Button onClick={handleCollapsed} className='custom-collapseBtn'>
    {collapsed ? <AlignRightOutlined /> : <AlignLeftOutlined />}
   </Button>
  );
 };
 return (
  <div>
   <Layout>
    <Sider
     className='custom-sideBar'
     trigger={null}
     collapsible
     collapsed={collapsed}
     id='sideBar1'
    >
     <div className={styles["logo-bar"]}>
      <img src={logo} alt='Logo' />
     </div>
     <Menu
      className='custom-sideBar-menu'
      mode='inline'
      defaultSelectedKeys={["1"]}
     >
      <div className='hide-text'>
       <h2 className={styles["menu-text"]}>DASHBOARD</h2>
      </div>
      <Menu.Item key='1' icon={<HomeOutlined />}>
       Dashboard
      </Menu.Item>
      <SubMenu key='sub1' icon={<WalletOutlined />} title='crypto currencies'>
       <Menu.Item key='opt1'>option1</Menu.Item>
       <Menu.Item key='opt2'>option2</Menu.Item>
       <Menu.Item key='opt3'>option3</Menu.Item>
       <Menu.Item key='opt4'>option4</Menu.Item>
      </SubMenu>
      <SubMenu key='sub2' icon={<ShoppingCartOutlined />} title='E-Commerce'>
       <Menu.Item key='opt-sub-1'>option1</Menu.Item>
       <Menu.Item key='opt-sub-2'>option2</Menu.Item>
       <Menu.Item key='opt-sub-3'>option3</Menu.Item>
       <Menu.Item key='opt-sub-4'>option4</Menu.Item>
      </SubMenu>
      <div className='hide-text'>
       <h2 className={styles["menu-text-two"]}>DASHBOARD</h2>
      </div>
      <Menu.Item key='3' icon={<AppstoreOutlined />}>
       Widgets
      </Menu.Item>
      <Menu.Item key='4' icon={<MailOutlined />}>
       Mail
       <span className={styles["custom-counter"]}>
        <Badge className='custom-badge' count={3}></Badge>
       </span>
      </Menu.Item>
     </Menu>
    </Sider>
    <Layout className='site-layout'>
     <Header className={styles["custom-top-header"]}>
      <TopHeader toggler={togglerBtn} />
     </Header>
     <Content className={styles["custom-content"]}>{props.children}</Content>
    </Layout>
   </Layout>
  </div>
 );
};

export default SideBar;
