import React from "react";
import SideBar from "./../sidebr/SideBar";
import styles from "./styles.module.scss";
import BreadCrum from "./BreadCrum";
import dashboard_img from "../../assets/images/dasboard-img.PNG";
import { Card } from "antd";
import { DollarOutlined } from "@ant-design/icons";
const Dashboard = () => {
 const cards = [
  {
   id: 1,
   heading: "Total Revenue",
   shortText: "Previos month vs this month",
   money: "$5, 900.00",
   per: "55%",
   Staus: "Higher",
  },
  {
   id: 2,
   heading: "New Employees",
   shortText: "Employees joined this month",
   money: "15",
   per: "5%",
   Staus: "increased",
  },
  {
   id: 3,
   heading: "Total Expenses",
   shortText: "Previos month vs this month",
   money: "$8,500",
   per: "12%",
   Staus: "increased",
  },
 ];
 return (
  <div>
   <SideBar>
    <h1 className={styles["welcome-text"]}>Welcome To Dashboard</h1>
    <div>
     <BreadCrum />
    </div>
    <div className={styles["slider-div"]}>
     <img src={dashboard_img} alt='Dashboard img' />
    </div>
    <div className={styles["grid-row"]}>
     {cards.map((item) => (
      <div key={item.id}>
       <Card className='custom-card'>
        <h2 className={styles["total-revenue"]}>{item.heading}</h2>
        <h4 className={styles["short-text"]}>{item.shortText}</h4>
        <div className={styles["flex-div"]}>
         <div>
          <h2 className={styles["money-text"]}>{item.money}</h2>
          <h4 className={styles["text-green"]}>
           {item.per} <span className={styles["text-muted"]}>{item.Staus}</span>
          </h4>
         </div>
         <div>
          <span className={styles["icon"]}>
           <DollarOutlined />
          </span>
         </div>
        </div>
       </Card>
      </div>
     ))}
    </div>
   </SideBar>
  </div>
 );
};

export default Dashboard;
