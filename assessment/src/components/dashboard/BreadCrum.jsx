import React from "react";
import { Breadcrumb } from "antd";
import { Link } from "react-router-dom";
const BreadCrum = () => {
 return (
  <div>
   <Breadcrumb className='custom-breadCrum'>
    <Breadcrumb.Item>
     <Link to='/'>Home</Link>
    </Breadcrumb.Item>
    <Breadcrumb.Item>
     <Link to='/'>Project Dashboard</Link>
    </Breadcrumb.Item>
   </Breadcrumb>
  </div>
 );
};

export default BreadCrum;
